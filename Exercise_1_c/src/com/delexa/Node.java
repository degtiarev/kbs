package com.delexa;

import java.util.ArrayList;


public class Node {

    String label;
    ArrayList<Node> adjList = new ArrayList<Node>();
    ArrayList<Integer> cost = new ArrayList<Integer>();


    Node bestAdj() {

        int cheap = cheapest(this.adjList, this.cost);
        return adjList.get(cheap);
    }

    int cheapest(ArrayList<Node> node, ArrayList<Integer> costs) {

        int ans = 0;

        for (int i = 1; i < costs.size(); i++) {

            if (costs.get(i) > costs.get(i - 1)) {

                ans = i - 1;
            } else {
                ans = i;
            }
        }

        return ans;

    }

}
