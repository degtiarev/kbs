package com.delexa;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    static List<List<Integer>> graph = readFile("input.txt");

    public static void main(String[] args) {


//        for (int i = 0; i < graph.size(); i++) {
//            for (int j = 0; j < graph.size(); j++) {
//
//            }
//        }

        Node n1 = new Node();
        n1.label = "A";
        Node n2 = new Node();
        n2.label = "B";
        Node n3 = new Node();
        n3.label = "C";
        Node n4 = new Node();
        n4.label = "D";
        Node n5 = new Node();
        n5.label = "E";


        n1.adjList.add(n2);
        //n1.cost.add(5);
        n1.cost.add(1);
        n1.adjList.add(n3);
        //n1.cost.add(2);
        n1.cost.add(1);

//        n1.adjList.add(n5);
//        n1.cost.add(1);


        n2.adjList.add(n4);
        n2.cost.add(1);


        n3.adjList.add(n4);
        // n3.cost.add(3);
        n3.cost.add(1);
        n3.adjList.add(n5);
        //n3.cost.add(5);
        n3.cost.add(1);


        n4.adjList.add(n5);
        n4.cost.add(1);
        //  n4.cost.add(3);


        Node n = n1;
        System.out.print(n.label);
        while (n.label != "E") {
            n = n.bestAdj();
            System.out.print(" -> " + n.label);
        }

    }

    static public List<List<Integer>> readFile(String fileName) {

        File file = new File(fileName);
        List<List<Integer>> graph = new ArrayList<>();

        try {

            Scanner fileScanner = new Scanner(file);

            while (fileScanner.hasNextLine()) {

                String line = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                List<Integer> lineList = new ArrayList<>();

                while (lineScanner.hasNext()) {
                    int i = lineScanner.nextInt();
                    lineList.add(i);
//                    System.out.print(i+" ");
                }
//                 System.out.println();
                graph.add(lineList);
                lineScanner.close();
            }
            fileScanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Error, no such file!");
        }

        return graph;

    }
}
