package com.delexa;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    static List<List<Integer>> graph = readFile("input.txt");
    static boolean[] used = new boolean[graph.size()];
    static List<Integer> result = new ArrayList<>();

    public static void main(String[] args) {
        dfs(1);

        for (int i=0; i<result.size(); i++)
        {
            System.out.print(result.get(i));

            if (result.get(i)==47)
                break;

            System.out.print(" ");
        }
    }

    static public void dfs(int pos) {
        used[pos] = true;
        result.add(pos + 1);

        List<Integer> currentNode = graph.get(pos);

        for (int i = 0; i < currentNode.size(); i++) {

            if (currentNode.get(i) == 1 && used[i] == false)
                dfs(i);
        }

    }

    static public List<List<Integer>> readFile(String fileName) {

        File file = new File(fileName);
        List<List<Integer>> graph = new ArrayList<>();

        try {

            Scanner fileScanner = new Scanner(file);

            while (fileScanner.hasNextLine()) {

                String line = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                List<Integer> lineList = new ArrayList<>();

                while (lineScanner.hasNext()) {
                    int i = lineScanner.nextInt();
                    lineList.add(i);
//                    System.out.print(i+" ");
                }
//                 System.out.println();
                graph.add(lineList);
                lineScanner.close();
            }
            fileScanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Error, no such file!");
        }

        return graph;

    }

}