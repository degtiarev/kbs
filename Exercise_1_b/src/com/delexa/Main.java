package com.delexa;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    static List<List<Integer>> graph = readFile("input.txt");
    static Queue<Integer> queue = new LinkedList<Integer>();
    static List<Integer> result = new ArrayList<>();


    public static void main(String[] args) {

        bfs(1);

        for (int i = 0; i < result.size(); i++) {
            System.out.print(result.get(i));

            if (result.get(i)==47)
                break;
            System.out.print(" ");
        }

    }


    static public void bfs(int source) {

        int number_of_nodes = graph.get(source).size() ;

        boolean[] visited = new boolean[number_of_nodes];
        int i, element;

        visited[source] = true;
        queue.add(source);

        while (!queue.isEmpty()) {
            element = queue.remove(); // removes from head
            i = 0;

            result.add(element+1);
            while (i < number_of_nodes) {
                if (graph.get(element).get(i) == 1 && visited[i] == false) {
                    queue.add(i);
                    visited[i] = true;
                }
                i++;
            }
        }


    }

    static public List<List<Integer>> readFile(String fileName) {

        File file = new File(fileName);
        List<List<Integer>> graph = new ArrayList<>();

        try {

            Scanner fileScanner = new Scanner(file);

            while (fileScanner.hasNextLine()) {

                String line = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                List<Integer> lineList = new ArrayList<>();

                while (lineScanner.hasNext()) {
                    int i = lineScanner.nextInt();
                    lineList.add(i);
//                    System.out.print(i+" ");
                }
//                 System.out.println();
                graph.add(lineList);
                lineScanner.close();
            }
            fileScanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Error, no such file!");
        }

        return graph;

    }
}
